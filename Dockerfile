# Use a base image that includes Node.js
FROM node:14

# Set the working directory inside the container
WORKDIR /app

# Copy your application code to the container
COPY . /app

# Install dependencies for your backend application
RUN npm install

# Expose port 2000 on which your application will listen
EXPOSE 2000

# Define the command to start your backend application
CMD ["npm", "start"]
