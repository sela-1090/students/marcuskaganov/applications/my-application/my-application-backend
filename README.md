# my-application-backend


Welcome to the My Application Backend. This README provides instructions on how to perform the Continuous Integration (CI) steps manually.

## CI Steps

1. **Build the Dockerfile:**

   To build the Docker image of your application, follow these steps:

   - Open a terminal or command prompt.

   - Navigate to the root directory of your application where the "Dockerfile" is located.

   - Run the following command to build the Docker image:

     ```bash
     docker build -t your-dockerhub-username/my-application-backend:<version> .
     ```

     Replace `your-dockerhub-username` with your DockerHub username and `<version>` with the version number you want to assign to the Docker image.

   - Wait for the build process to complete. Once finished, you should see a message indicating a successful build.

   - To verify that the image has been created, you can use the following command to list all Docker images on your system:

     ```bash
     docker images
     ```

   - You should see your newly created image listed in the output.

   Now you have successfully built the Docker image for your application, and you can proceed with the next CI steps.
