pipeline {
    agent {
        kubernetes {
            label 'promo-app'
            idleMinutes 5
            yamlFile 'build-pod.yaml'
            defaultContainer 'my-application-backend-docker-helm-build' // Update the defaultContainer field
        }
    }
    options {
        skipDefaultCheckout() // Skip the default Git checkout step as we'll do it manually later
    }
    stages {
        stage('Checkout') {
            steps {
                // Manually checkout the code from GitLab (replace <YOUR_GIT_REPO_URL> with the actual URL)
                checkout([$class: 'GitSCM',
                    branches: [[name: '*/main']], // Specify the branch you want to build
                    userRemoteConfigs: [[url: '<YOUR_GIT_REPO_URL>']],
                    extensions: [[$class: 'CleanBeforeCheckout']] // Clean the workspace before checkout
                ])
            }
        }
        stage('Build image') {
            steps {
                echo 'Starting to build docker image'
                sh "docker build -t my-application-backend ."
            }
        }
        stage('Run tests') {
            steps {
                echo 'Running tests...'
                // Add your test commands here
            }
        }
        stage('Push image to Docker registry') {
            steps {
                echo 'Pushing the Docker image to the registry...'
                // Replace <YOUR_DOCKER_REGISTRY> with your actual Docker registry URL
                sh "docker tag my-application-backend <YOUR_DOCKER_REGISTRY>/my-application-backend:latest"
                sh "docker push <YOUR_DOCKER_REGISTRY>/my-application-backend:latest"
            }
        }
    }
    post {
        success {
            echo "Pipeline executed successfully!"
        }
        failure {
            echo "Pipeline failed! Check the logs for more details."
        }
        always {
            echo "Cleaning up workspace..."
            deleteDir() // Clean up the workspace after the build
        }
    }
}
